<div align="center">
      <img src="https://cultofthepartyparrot.com/parrots/hd/parrot.gif" width="70" height="70"/>
<h1> Hi there 👋</h1>
</div>
🔭 <b>I’m currently working on:</b> too many sideprojects @ github<br />
🌱 <b>I’m currently learning:</b> fsharp<br />
🧪 <b>I’m experimenting with:</b> dotnet MAUI<br />
🥳 <b>I’m hyped about:</b> WebAssembly<br />
💬 <b>Ask me about:</b> remote & nomadic work<br />
📫 <b>How to reach me:</b> <a href="https://www.johnnys.page">johnnys.page</a><br />
📰 <b>How to read my blog:</b> <a href="https://www.johnnys.news">johnnys.news</a><br />
👤 <b>Pronouns:</b> he / him<br />
⚡️ <b>Fun fact:</b> 95% of my sideprojects are stuck at the last 5%<br />
<div align="center">
      <img src="https://cultofthepartyparrot.com/parrots/hd/reverseparrot.gif" width="70" height="70"/>
</div>
